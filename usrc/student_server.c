// --------------------------------------------------
//  EXAMPLE.  Replace it withyour project code      |
// --------------------------------------------------
/* In this example, the server receives command line arguments
 * from the client sent using the following packet format
 *    ----------------------------------------
 *   | nb | command | parameter1 | parameter2 |
 *    ----------------------------------------
 * where 
 * - nb (1 byte) is the number of parameters
 * - command (16 bytes) is a (null-terminated) string
 * - parameter1 (32 bytes) is a (null-terminated) string
 * - parameter2 (32 bytes) is a (null-terminated) string
 * irrelevant parameters are skipped
 * (w.r.t nb, depending of the command )
 * 
 * Once a packet has been parsed, the server writes the received arguments 
 * int its terminal
 */

// INCLUDES
/* user defined library for sending and receiving packets */
#include "../uinclude/communication.h"
/* for printf,...*/
#include <stdio.h>
/* Used to modify SIGPIPE signal handling, as the default behaviour makes
 * the program exits when trying to write to a closed socket. 
 * We don't want thi :-).
 */
#include <signal.h>

// FUNCTIONS
// ...
// Empty because all functions used in example are provided by 
// "provided", "communication" and system provided libraries.
// --------------------------------------------------
//  END of EXAMPLE code to be replaced              |
// --------------------------------------------------


// !! This is the function you must implement for your project.
// Here we provide an implementation related to the example, not
// to the project... return makes the server wait for next client.
void student_server(int channel, int argc, char *argv[]) {
    // Writing to a closed socket causes a SIGPIPE signal, which makes 
    // the program exits. The following line prevents this default behaviour.
    // Thus, writing to a closed socket makes the program simply return -1, put the EPIPE
    // in errno: this avoids the program to directly exit.
    // (cf the line with "EPIPE" in send_pkt in usrc/communication.c).
    signal(SIGPIPE, SIG_IGN);

    // --------------------------------------------------
    //  EXAMPLE.  To be replaced by your project code   |
    // --------------------------------------------------

    // buffer to receive packets (max size: 81)
    char recvbuf[81];
    // infinite loop -> use ^C to exit the program
    while (1) {
        // get the command from user, exit if it fails
        printf(" -- wait a packet (^C to exit) --\n");
        // get the packet
        int res = recv_pkt(recvbuf, channel);
        if (!res) return; // return if communication error occured
        //prints the command, relying on the number of parameters
        if (*recvbuf == 0) 
            printf("     command: %s\n", recvbuf+1);
        else if (*recvbuf == 1) 
            printf("     command: %s   param: %s\n", recvbuf+1, recvbuf+17);
        else
            printf("     command: %s   param1: %s   param2: %s\n", recvbuf+1, recvbuf+17, recvbuf+49);
    }
    // -----------------------------------------------------
    //  END of EXAMPLE code to be replace by your own code |
    // -----------------------------------------------------
}

